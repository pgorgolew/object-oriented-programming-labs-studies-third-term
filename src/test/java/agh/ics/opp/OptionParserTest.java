package agh.ics.opp;

import agh.ics.oop.MoveDirection;
import agh.ics.oop.OptionsParser;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OptionParserTest {
    private static final MoveDirection f = MoveDirection.FORWARD;
    private static final MoveDirection b = MoveDirection.BACKWARD;
    private static final MoveDirection r = MoveDirection.RIGHT;
    private static final MoveDirection l = MoveDirection.LEFT;

    @Test
    public void TestEmptyCommandLineArguments(){
        String[] testedArray = new String[0];
        ArrayList<MoveDirection> expectedResult = new ArrayList<>();
        assertEquals(expectedResult, OptionsParser.parse(testedArray));
    }

    @Test
    public void TestCommandLineArgumentsWithOnlyDefineShorts(){
        String[] testedArray = {"f","f","b","r","l","b","l","r"};
        ArrayList<MoveDirection> expectedResult = new ArrayList<>(Arrays.asList(f,f,b,r,l,b,l,r));
        assertEquals(expectedResult, OptionsParser.parse(testedArray));
    }

    @Test
    public void TestCommandLineArgumentsWithOnlyDefineLongNames(){
        String[] testedArray = {"forward","forward","backward","right","left","backward","left","right"};
        ArrayList<MoveDirection> expectedResult = new ArrayList<>(Arrays.asList(f,f,b,r,l,b,l,r));
        assertEquals(expectedResult, OptionsParser.parse(testedArray));
    }

    @Test
    public void TestCommandLineArgumentsWhenMixed(){
        String[] testedArray = {"forward","f","backward","r","left","b","l","right"};
        ArrayList<MoveDirection> expectedResult = new ArrayList<>(Arrays.asList(f,f,b,r,l,b,l,r));
        assertEquals(expectedResult, OptionsParser.parse(testedArray));
    }

    @Test
    public void TestCommandLineArgumentsWithSomeWrongMoves(){
        String[] testedArray = {"f","go_go","f","b","x","r","g","l","look","b","l","gora","r"};
        assertThrows(IllegalArgumentException.class, () -> OptionsParser.parse(testedArray));
    }

}
