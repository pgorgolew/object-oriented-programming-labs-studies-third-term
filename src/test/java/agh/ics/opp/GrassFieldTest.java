package agh.ics.opp;

import agh.ics.oop.*;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class GrassFieldTest {
    @Test
    public void testCanPlace() {
        GrassField testedMap = new GrassField(10);
        Grass randomGrass = testedMap.getAllGrasses().get(0);

        assertTrue(testedMap.place(new Animal(testedMap, new Vector2d(100, 100))));
        assertThrows(IllegalArgumentException.class, () -> testedMap.place(new Animal(testedMap, new Vector2d(100, 100))));
        assertTrue(testedMap.place(new Animal(testedMap, new Vector2d(101, 101))));
        assertTrue(testedMap.place(new Animal(testedMap, randomGrass.getPosition()))); //can place on grass
    }

    @Test
    public void testIsOcuppied() {
        GrassField testedMap = new GrassField(10);
        testedMap.place(new Animal(testedMap, new Vector2d(2, 2)));
        testedMap.place(new Animal(testedMap, new Vector2d(4, 4)));
        Grass randomGrass = testedMap.getAllGrasses().get(0);

        assertTrue(testedMap.isOccupied(new Vector2d(2, 2)));
        assertFalse(testedMap.isOccupied(new Vector2d(100, 100))); //to large number for grass so must be null
        assertTrue(testedMap.isOccupied(new Vector2d(4, 4)));
        assertTrue(testedMap.isOccupied(randomGrass.getPosition())); //grass
    }

    @Test
    public void testObjectAt() {
        GrassField testedMap = new GrassField(10);
        Animal testedAnimal = new Animal(testedMap, new Vector2d(2, 2));
        testedMap.place(testedAnimal);
        Grass randomGrass = testedMap.getAllGrasses().get(0);

        assertEquals(testedAnimal, testedMap.objectAt(new Vector2d(2, 2))); //animal
        assertEquals(randomGrass, testedMap.objectAt(randomGrass.getPosition())); //grass
        assertNull(testedMap.objectAt(new Vector2d(100, 100)));//to large numbers for grass so must be null
    }

    @Test
    public void testCanMoveTo() {
        GrassField testedMap = new GrassField(10);
        Animal testedAnimal = new Animal(testedMap, new Vector2d(2, 2));
        Animal testedAnimal2 = new Animal(testedMap, new Vector2d(2, 1));
        testedMap.place(testedAnimal);
        testedMap.place(testedAnimal2);

        assertFalse(testedMap.canMoveTo(new Vector2d(2, 2)));
        assertFalse(testedMap.canMoveTo(new Vector2d(2, 1)));
        assertTrue(testedMap.canMoveTo(new Vector2d(2, 3)));
    }
}
