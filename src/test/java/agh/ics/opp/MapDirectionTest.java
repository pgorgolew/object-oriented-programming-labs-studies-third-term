package agh.ics.opp;

import agh.ics.oop.MapDirection;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapDirectionTest {
    @Test
    public void nextNORTH(){
        MapDirection expected = MapDirection.EAST;
        assertEquals(expected, MapDirection.NORTH.next());
    }

    @Test
    public void nextSOUTH(){
        MapDirection expected = MapDirection.WEST;
        assertEquals(expected, MapDirection.SOUTH.next());
    }

    @Test
    public void nextWEST(){
        MapDirection expected = MapDirection.NORTH;
        assertEquals(expected, MapDirection.WEST.next());
    }

    @Test
    public void nextEAST(){
        MapDirection expected = MapDirection.SOUTH;
        assertEquals(expected, MapDirection.EAST.next());
    }

    @Test
    public void previousNORTH(){
        MapDirection expected = MapDirection.WEST;
        assertEquals(expected, MapDirection.NORTH.previous());
    }

    @Test
    public void previousSOUTH(){
        MapDirection expected = MapDirection.EAST;
        assertEquals(expected, MapDirection.SOUTH.previous());
    }

    @Test
    public void previousWEST(){
        MapDirection expected = MapDirection.SOUTH;
        assertEquals(expected, MapDirection.WEST.previous());
    }

    @Test
    public void previousEAST(){
        MapDirection expected = MapDirection.NORTH;
        assertEquals(expected, MapDirection.EAST.previous());
    }
}
