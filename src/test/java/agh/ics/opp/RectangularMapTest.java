package agh.ics.opp;

import agh.ics.oop.Animal;
import agh.ics.oop.RectangularMap;
import agh.ics.oop.Vector2d;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RectangularMapTest {
    @Test
    public void testCanPlace() {
        RectangularMap testedMap = new RectangularMap(10, 10);

        assertTrue(testedMap.place(new Animal(testedMap, new Vector2d(6, 3))));
        assertThrows(IllegalArgumentException.class, () -> testedMap.place(new Animal(testedMap, new Vector2d(6, 3))));
        assertTrue(testedMap.place(new Animal(testedMap, new Vector2d(3, 3))));
    }

    @Test
    public void testIsOcuppied() {
        RectangularMap testedMap = new RectangularMap(10, 10);
        testedMap.place(new Animal(testedMap, new Vector2d(2, 2)));
        testedMap.place(new Animal(testedMap, new Vector2d(4, 4)));

        assertTrue(testedMap.isOccupied(new Vector2d(2, 2)));
        assertFalse(testedMap.isOccupied(new Vector2d(2, 6)));
        assertTrue(testedMap.isOccupied(new Vector2d(4, 4)));
    }

    @Test
    public void testObjectAt() {
        RectangularMap testedMap = new RectangularMap(10, 10);
        Animal testedAnimal = new Animal(testedMap, new Vector2d(2, 2));
        testedMap.place(testedAnimal);

        assertEquals(testedAnimal, testedMap.objectAt(new Vector2d(2, 2)));
        assertNull(testedMap.objectAt(new Vector2d(2, 7)));
    }

    @Test
    public void testCanMoveTo() {
        RectangularMap testedMap = new RectangularMap(10, 10);
        Animal testedAnimal = new Animal(testedMap, new Vector2d(2, 2));
        Animal testedAnimal2 = new Animal(testedMap, new Vector2d(2, 1));
        testedMap.place(testedAnimal);
        testedMap.place(testedAnimal2);

        assertFalse(testedMap.canMoveTo(new Vector2d(2, 2)));
        assertFalse(testedMap.canMoveTo(new Vector2d(2, 1)));
        assertFalse(testedMap.canMoveTo(new Vector2d(10, 10)));
        assertFalse(testedMap.canMoveTo(new Vector2d(-1, 0)));
        assertTrue(testedMap.canMoveTo(new Vector2d(5, 1)));
    }

    @Test
    public void testIfMapSizeAreGood() {
        RectangularMap testedMap = new RectangularMap(10, 5);
        assertEquals(testedMap.getHeight(), 5);
        assertEquals(testedMap.getWidth(), 10);
    }
}