package agh.ics.opp;

import agh.ics.oop.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SimulationEngineTest {
    @Test
    void TestAddingAnimals() {
        List<MoveDirection> directions = OptionsParser.parse(new String[]{"f", "f"});
        AbstractWorldMap map = new RectangularMap(10, 5);
        List<Vector2d> positions = new ArrayList<>(Arrays.asList(new Vector2d(2,2), new Vector2d(8,4)));
        SimulationEngine engine = new SimulationEngine(directions, map, positions, null);

        assertEquals(2, engine.getAllAnimals().size());
        assertEquals(new Vector2d(2, 2), engine.getAllAnimals().get(0).getPosition());
        assertEquals(new Vector2d(8, 4), engine.getAllAnimals().get(1).getPosition());
    }

    @Test
    void TestIfAnimalsAreOnMap() {
        List<MoveDirection> directions = OptionsParser.parse(new String[]{"f", "f"});
        AbstractWorldMap map = new RectangularMap(10, 5);
        List<Vector2d> positions = new ArrayList<>(Arrays.asList(new Vector2d(2,2), new Vector2d(8,4)));
        SimulationEngine engine = new SimulationEngine(directions, map, positions, null);

        assertTrue(engine.getMap().isOccupied(new Vector2d(2,2)));
        assertTrue(engine.getMap().isOccupied(new Vector2d(8,4)));
    }

    @Test
    void TestRunDeafult() {
        List<MoveDirection> directions = OptionsParser.parse(new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f", "f", "f", "f", "f", "f", "f"});
        AbstractWorldMap map = new RectangularMap(10, 5);
        List<Vector2d> positions = new ArrayList<>(Arrays.asList(new Vector2d(2,2), new Vector2d(3,4)));
        SimulationEngine engine = new SimulationEngine(directions, map, positions, null);
        engine.run();

        assertEquals(new Vector2d(2, 0), engine.getAllAnimals().get(0).getPosition());
        assertEquals(new Vector2d(3, 4), engine.getAllAnimals().get(1).getPosition());
    }

    @Test
    void testRunTestForThreeAnimals() {
        List<MoveDirection> directions = OptionsParser.parse(new String[]{"r","l","f","f","f","f","b","r","l","b","f","f","b","f","f","r","f","f","f","f","f","f","f","f","f","f","l","f","f","f"});
        AbstractWorldMap map = new RectangularMap(11, 11);
        List<Vector2d> positions = new ArrayList<>(Arrays.asList(new Vector2d(1,3), new Vector2d(3,3), new Vector2d(8,9)));
        SimulationEngine engine = new SimulationEngine(directions, map, positions, null);
        engine.run();

        assertEquals(new Vector2d(0, 0), engine.getAllAnimals().get(0).getPosition());
        assertEquals(new Vector2d(3, 9), engine.getAllAnimals().get(1).getPosition());
        assertEquals(new Vector2d(3, 9), engine.getAllAnimals().get(1).getPosition());
    }

    @Test
    void testBigOne() {
        List<MoveDirection> directions = OptionsParser.parse(new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f",
                "f", "f", "f", "f", "f", "f", "f", "l", "r", "f", "b", "b", "b", "b", "b", "l", "l", "f", "r", "l", "l",
                "f", "b", "b", "l", "f", "b", "l", "f", "f", "f", "r", "r", "l", "l", "f", "l", "l", "f", "f", "b", "l",
                "b", "l", "l", "l", "b", "l", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"});
        AbstractWorldMap map = new RectangularMap(10, 15);
        List<Vector2d> positions = new ArrayList<>(Arrays.asList(new Vector2d(2,2), new Vector2d(3,10),
                new Vector2d(5,5), new Vector2d(7,12), new Vector2d(6,14), new Vector2d(9,13)));
        SimulationEngine engine = new SimulationEngine(directions, map, positions, null);
        engine.run();

        assertEquals(new Vector2d(2, 3), engine.getAllAnimals().get(0).getPosition());
        assertEquals(new Vector2d(7, 9), engine.getAllAnimals().get(1).getPosition());
        assertEquals(new Vector2d(9, 5), engine.getAllAnimals().get(2).getPosition());
        assertEquals(new Vector2d(5, 14), engine.getAllAnimals().get(3).getPosition());
        assertEquals(new Vector2d(5, 13), engine.getAllAnimals().get(4).getPosition());
        assertEquals(new Vector2d(9, 11), engine.getAllAnimals().get(5).getPosition());
    }
}
