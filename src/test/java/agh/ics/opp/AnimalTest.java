package agh.ics.opp;

import agh.ics.oop.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnimalTest {
    private static final MapDirection n = MapDirection.NORTH;
    private static final MapDirection s = MapDirection.SOUTH;
    private static final MapDirection w = MapDirection.WEST;
    private static final MapDirection e = MapDirection.EAST;

    //first unit tests
    @Test
    public void testIfProperDefaultPosition(){
        Animal testedAnimal = new Animal();
        assertEquals(new Vector2d(2,2), testedAnimal.getPosition());
    }
    @Test
    public void testIfNorthIsDefault(){
        Animal testedAnimal = new Animal();
        assertEquals(MapDirection.NORTH, testedAnimal.getOrient());
    }

    @Test
    public void testRightNorth(){
        Animal testedAnimal = new Animal();
        testedAnimal.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.EAST, testedAnimal.getOrient());
    }

    @Test
    public void testRightSouth(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.SOUTH);
        testedAnimal.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.WEST, testedAnimal.getOrient());
    }

    @Test
    public void testRightWest(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.WEST);
        testedAnimal.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.NORTH, testedAnimal.getOrient());
    }

    @Test
    public void testRightEast(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.EAST);
        testedAnimal.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.SOUTH, testedAnimal.getOrient());
    }

    @Test
    public void testLeftNorth(){
        Animal testedAnimal = new Animal();
        testedAnimal.move(MoveDirection.LEFT);
        assertEquals(MapDirection.WEST, testedAnimal.getOrient());
    }

    @Test
    public void testLeftSouth(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.SOUTH);
        testedAnimal.move(MoveDirection.LEFT);
        assertEquals(MapDirection.EAST, testedAnimal.getOrient());
    }

    @Test
    public void testLeftWest(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.WEST);
        testedAnimal.move(MoveDirection.LEFT);
        assertEquals(MapDirection.SOUTH, testedAnimal.getOrient());
    }

    @Test
    public void testLeftEast(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.EAST);
        testedAnimal.move(MoveDirection.LEFT);
        assertEquals(MapDirection.NORTH, testedAnimal.getOrient());
    }

    @Test
    public void testForwardNorth(){
        Animal testedAnimal = new Animal();
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,3), testedAnimal.getPosition());
    }

    @Test
    public void testForwardSouth(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.SOUTH);
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,1), testedAnimal.getPosition());
    }

    @Test
    public void testForwardWest(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.WEST);
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(1,2), testedAnimal.getPosition());
    }

    @Test
    public void testForwardEast(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.EAST);
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(3,2), testedAnimal.getPosition());
    }

    @Test
    public void testBackwardNorth(){
        Animal testedAnimal = new Animal();
        testedAnimal.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,1), testedAnimal.getPosition());
    }

    @Test
    public void testBackwardSouth(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.SOUTH);
        testedAnimal.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,3), testedAnimal.getPosition());
    }

    @Test
    public void testBackwardWest(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.WEST);
        testedAnimal.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(3,2), testedAnimal.getPosition());
    }

    @Test
    public void testBackwardEast(){
        Animal testedAnimal = new Animal();
        testedAnimal.setOrient(MapDirection.EAST);
        testedAnimal.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(1,2), testedAnimal.getPosition());
    }

    @Test
    public void testIfStayInMapForXInEast(){
        Animal testedAnimal = new Animal();
        testedAnimal.setPosition(new Vector2d(4,3));
        testedAnimal.setOrient(MapDirection.EAST);
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(4,3), testedAnimal.getPosition());
    }

    @Test
    public void testIfStayInMapForXInWest(){
        Animal testedAnimal = new Animal();
        testedAnimal.setPosition(new Vector2d(0,3));
        testedAnimal.setOrient(MapDirection.WEST);
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(0,3), testedAnimal.getPosition());
    }

    @Test
    public void testIfStayInMapForYInNorth(){
        Animal testedAnimal = new Animal();
        testedAnimal.setPosition(new Vector2d(2,4));
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,4), testedAnimal.getPosition());
    }

    @Test
    public void testIfStayInMapForYInSouth(){
        Animal testedAnimal = new Animal();
        testedAnimal.setPosition(new Vector2d(2,0));
        testedAnimal.setOrient(MapDirection.SOUTH);
        testedAnimal.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,0), testedAnimal.getPosition());
    }

    @Test
    public void testGivenArgsWithoutGoingOutFromTheMap(){
        Animal testedAnimal = new Animal();
        String[] testedCommandLineArgs = {"f", "forward", "backward", "right", "f", "r", "b", "l"};
        testedAnimal.move(OptionsParser.parse(testedCommandLineArgs));

        MapDirection expectedOrient = MapDirection.EAST;
        Vector2d expectedPosition = new Vector2d(3,4);

        assertEquals(expectedOrient, testedAnimal.getOrient());
        assertEquals(expectedPosition, testedAnimal.getPosition());
    }

    @Test
    public void testGivenArgsWithGoingOutFromTheMap(){
        Animal testedAnimal = new Animal();
        String[] testedCommandLineArgs = {"f", "f", "f", "b", "l", "f", "f", "f", "l", "b", "b"};
        testedAnimal.move(OptionsParser.parse(testedCommandLineArgs));

        MapDirection expectedOrient = MapDirection.SOUTH;
        Vector2d expectedPosition = new Vector2d(0,4);

        assertEquals(expectedOrient, testedAnimal.getOrient());
        assertEquals(expectedPosition, testedAnimal.getPosition());
    }

    @Test
    public void testGivenArgsWhen10TimesForward(){
        Animal testedAnimal = new Animal();
        String[] testedCommandLineArgs = {"f", "f", "f", "f", "f", "f", "f", "f", "f", "f"};
        testedAnimal.move(OptionsParser.parse(testedCommandLineArgs));

        MapDirection expectedOrient = MapDirection.NORTH;
        Vector2d expectedPosition = new Vector2d(2,4);

        assertEquals(expectedOrient, testedAnimal.getOrient());
        assertEquals(expectedPosition, testedAnimal.getPosition());
    }

    @Test
    public void testEachMoveWhenGivenCommandLineArgs(){
        Animal testedAnimal = new Animal();
        String[] testedCommandLineArgs = {"f", "f", "f", "b", "l", "f", "f", "f", "l", "b", "b"};
        List<MoveDirection> argsAfterParser = OptionsParser.parse(testedCommandLineArgs);

        List<MapDirection> expectedOrient = new ArrayList<>(Arrays.asList(n,n,n,n,w,w,w,w,s,s,s));
        int[] expectedX = {2,2,2,2,2,1,0,0,0,0,0};
        int[] expectedY = {3,4,4,3,3,3,3,3,3,4,4};
        for(int i = 0; i < testedCommandLineArgs.length; i++){
            testedAnimal.move(argsAfterParser.get(i));

            assertEquals(expectedOrient.get(i), testedAnimal.getOrient());
            assertEquals(new Vector2d(expectedX[i], expectedY[i]), testedAnimal.getPosition());
        }
    }
}
