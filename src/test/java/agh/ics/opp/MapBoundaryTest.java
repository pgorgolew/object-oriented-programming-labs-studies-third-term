package agh.ics.oop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapBoundaryTest {
    @Test
    void getLowerLeft() {
        AbstractWorldMap map = new RectangularMap(10, 10);
        MapBoundary mapBoundary = new MapBoundary(map);

        mapBoundary.addElement(new Vector2d(2, 7));
        mapBoundary.addElement(new Vector2d(5, 4));

        assertEquals(new Vector2d(2, 4), mapBoundary.getLowerLeft());
    }

    @Test
    void getUpperRight() {
        AbstractWorldMap map = new RectangularMap(10, 10);
        MapBoundary mapBoundary = new MapBoundary(map);

        mapBoundary.addElement(new Vector2d(2, 7));
        mapBoundary.addElement(new Vector2d(5, 4));

        assertEquals(new Vector2d(5, 7), mapBoundary.getUpperRight());
    }
}