package agh.ics.opp;

import agh.ics.oop.MapDirection;
import agh.ics.oop.Vector2d;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class Vector2dTest {
    @Test
    public void equalsDifferentObjects(){
        assertFalse(new Vector2d(2,1).equals(MapDirection.NORTH));
    }

    @Test
    public void equalsSameObject(){
        Vector2d vectorToTest = new Vector2d(2,1);
        assertTrue(vectorToTest.equals(vectorToTest));
    }

    @Test
    public void equalsDifferentTwoValues(){
        assertFalse(new Vector2d(2,1).equals(new Vector2d(3,3)));
    }

    @Test
    public void equalsDifferentY(){
        assertFalse(new Vector2d(2,1).equals(new Vector2d(2,4)));
    }

    @Test
    public void equalsDifferentX(){
        assertFalse(new Vector2d(2,1).equals(new Vector2d(3,1)));
    }

    @Test
    public void equalSameTwoValues(){
        assertTrue(new Vector2d(2,1).equals(new Vector2d(2,1)));
    }

    @Test
    public void toStringTwoPositives(){
        String expected = "(2,1)";
        assertEquals(expected, new Vector2d(2,1).toString());
    }

    @Test
    public void toStringMixed(){
        String expected = "(-2,1)";
        assertEquals(expected, new Vector2d(-2,1).toString());
    }

    @Test
    public void toStringTwoNegatives(){
        String expected = "(-2,-1)";
        assertEquals(expected, new Vector2d(-2,-1).toString());
    }

    @Test
    public void precedesBothLower(){
        Vector2d firstVector = new Vector2d(1,1);
        Vector2d secondVector = new Vector2d(2,2);
        assertTrue(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesXLowerYEqual(){
        Vector2d firstVector = new Vector2d(1,1);
        Vector2d secondVector = new Vector2d(2,1);
        assertTrue(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesXEqualYLower(){
        Vector2d firstVector = new Vector2d(1,1);
        Vector2d secondVector = new Vector2d(1,2);
        assertTrue(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesBothEqual(){
        Vector2d firstVector = new Vector2d(1,1);
        Vector2d secondVector = new Vector2d(1,1);
        assertTrue(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesXEqualYGreater(){
        Vector2d firstVector = new Vector2d(1,2);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesXGreaterYEqual(){
        Vector2d firstVector = new Vector2d(2,1);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesBothGreater(){
        Vector2d firstVector = new Vector2d(2,2);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesXGreaterYLower(){
        Vector2d firstVector = new Vector2d(2,0);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.precedes(secondVector));
    }

    @Test
    public void precedesXLowerYGreater(){
        Vector2d firstVector = new Vector2d(2,0);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.precedes(secondVector));
    }

    @Test
    public void followsBothGreater(){
        Vector2d firstVector = new Vector2d(2,2);
        Vector2d secondVector = new Vector2d(1,1);
        assertTrue(firstVector.follows(secondVector));
    }

    @Test
    public void followsXGreaterYEqual(){
        Vector2d firstVector = new Vector2d(2,1);
        Vector2d secondVector = new Vector2d(1,1);
        assertTrue(firstVector.follows(secondVector));
    }

    @Test
    public void followsXEqualYGreater(){
        Vector2d firstVector = new Vector2d(1,2);
        Vector2d secondVector = new Vector2d(1,1);
        assertTrue(firstVector.follows(secondVector));
    }

    @Test
    public void followsBothEqual(){
        Vector2d firstVector = new Vector2d(1,1);
        Vector2d secondVector = new Vector2d(1,1);
        assertTrue(firstVector.follows(secondVector));
    }

    @Test
    public void followsXLowerYEqual(){
        Vector2d firstVector = new Vector2d(0,1);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.follows(secondVector));
    }

    @Test
    public void followsXEqualYLower(){
        Vector2d firstVector = new Vector2d(1,0);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.follows(secondVector));
    }

    @Test
    public void followsBothLower(){
        Vector2d firstVector = new Vector2d(0,0);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.follows(secondVector));
    }

    @Test
    public void followsXGreaterYLower(){
        Vector2d firstVector = new Vector2d(2,0);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.follows(secondVector));
    }

    @Test
    public void followsXLowerYGreater(){
        Vector2d firstVector = new Vector2d(0,2);
        Vector2d secondVector = new Vector2d(1,1);
        assertFalse(firstVector.follows(secondVector));
    }

    @Test
    public void upperRightXYFromFirst(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(2,4).upperRight(new Vector2d(1,1)));
    }

    @Test
    public void upperRightXFromFirstYFromSecond(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(2,3).upperRight(new Vector2d(1,4)));
    }

    @Test
    public void upperRightXFromSecondYFromFirst(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(1,4).upperRight(new Vector2d(2,0)));
    }

    @Test
    public void upperRightXYFromSecond(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(0,0).upperRight(new Vector2d(2,4)));
    }

    @Test
    public void upperRightWhenVectorsEquals(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(2,4).upperRight(new Vector2d(2,4)));
    }

    @Test
    public void upperRightXSameYDifferent(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(2,3).upperRight(new Vector2d(2,4)));
    }

    @Test
    public void upperRightYSameXDifferent(){
        Vector2d expected = new Vector2d(2,4);
        assertEquals(expected, new Vector2d(2,4).upperRight(new Vector2d(1,4)));
    }

    @Test
    public void lowerLeftXYFromFirst(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(0,0).lowerLeft(new Vector2d(1,1)));
    }

    @Test
    public void lowerLeftXFromSecondYFromFirst(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(1,0).lowerLeft(new Vector2d(0,8)));
    }

    @Test
    public void lowerLeftXYFromSecond(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(4,5).lowerLeft(new Vector2d(0,0)));
    }

    @Test
    public void lowerLeftWhenVectorsEquals(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(0,0).lowerLeft(new Vector2d(0,0)));
    }

    @Test
    public void lowerLeftXSameYDifferent(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(0,3).lowerLeft(new Vector2d(0,0)));
    }

    @Test
    public void lowerLeftYSameXDifferent(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(0,0).lowerLeft(new Vector2d(1,0)));
    }

    @Test
    public void lowerLeftXFromFirstYFromSecond(){
        Vector2d expected = new Vector2d(0,0);
        assertEquals(expected, new Vector2d(0,3).lowerLeft(new Vector2d(1,0)));
    }

    @Test
    public void addTwoPositives(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(0,0).add(new Vector2d(4,4)));
    }

    @Test
    public void addXPositiveYNegative(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(0,5).add(new Vector2d(4,-1)));
    }

    @Test
    public void addXNegativeYPositive(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(5,0).add(new Vector2d(-1,4)));
    }

    @Test
    public void addBothNegatives(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(5,5).add(new Vector2d(-1,-1)));
    }

    @Test
    public void addXPositiveYZero(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(3,4).add(new Vector2d(1,0)));
    }

    @Test
    public void addXZeroYPositive(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(4,3).add(new Vector2d(0,1)));
    }

    @Test
    public void addXZeroYNegative(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(4,5).add(new Vector2d(0,-1)));
    }

    @Test
    public void addXNegativeYZero(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(5,4).add(new Vector2d(-1,0)));
    }


    @Test
    public void addTwoZeros(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(4,4).add(new Vector2d(0,0)));
    }

    @Test
    public void subtractTwoPositives(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(5,5).subtract(new Vector2d(1,1)));
    }

    @Test
    public void subtractXPositiveYNegative(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(5,3).subtract(new Vector2d(1,-1)));
    }

    @Test
    public void subtractXNegativeYPositive(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(3,5).subtract(new Vector2d(-1,1)));
    }

    @Test
    public void subtractBothNegatives(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(3,3).subtract(new Vector2d(-1,-1)));
    }

    @Test
    public void subtractXPositiveYZero(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(5,4).subtract(new Vector2d(1,0)));
    }

    @Test
    public void subtractXZeroYPositive(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(4,5).subtract(new Vector2d(0,1)));
    }

    @Test
    public void subtractXZeroYNegative(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(4,3).subtract(new Vector2d(0,-1)));
    }

    @Test
    public void subtractXNegativeYZero(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(3,4).subtract(new Vector2d(-1,0)));
    }

    @Test
    public void subtractTwoZeros(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(4,4).subtract(new Vector2d(0,0)));
    }

    @Test
    public void oppositeTwoZeros(){
        Vector2d expected = new Vector2d(-4,-4);
        assertEquals(expected, new Vector2d(4,4).opposite());
    }

    @Test
    public void oppositeXZeroYPositive(){
        Vector2d expected = new Vector2d(0,-4);
        assertEquals(expected, new Vector2d(0,4).opposite());
    }

    @Test
    public void oppositeXZeroYNegative(){
        Vector2d expected = new Vector2d(0,4);
        assertEquals(expected, new Vector2d(0,-4).opposite());
    }
    @Test
    public void oppositeXPositiveYZero(){
        Vector2d expected = new Vector2d(-4,0);
        assertEquals(expected, new Vector2d(4,0).opposite());
    }
    @Test
    public void oppositeXNegativeYZero(){
        Vector2d expected = new Vector2d(4,0);
        assertEquals(expected, new Vector2d(-4,0).opposite());
    }
    @Test
    public void oppositeXPositiveYNegative(){
        Vector2d expected = new Vector2d(-4,4);
        assertEquals(expected, new Vector2d(4,-4).opposite());
    }
    @Test
    public void oppositeXNegativeYPositive(){
        Vector2d expected = new Vector2d(4,-4);
        assertEquals(expected, new Vector2d(-4,4).opposite());
    }
    @Test
    public void oppositeBothNegative(){
        Vector2d expected = new Vector2d(4,4);
        assertEquals(expected, new Vector2d(-4,-4).opposite());
    }
    @Test
    public void oppositeBothPositive() {
        Vector2d expected = new Vector2d(-4, -4);
        assertEquals(expected, new Vector2d(4, 4).opposite());
    }
}


