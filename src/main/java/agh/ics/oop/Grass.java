package agh.ics.oop;

import java.util.HashMap;

public class Grass extends AbstractWorldMapElement{

    public Grass(Vector2d position){
        this.position = position;
    }

    @Override
    public String toString() {return "*";}

    @Override
    public String getPathToImage() {
        return "src/main/resources/grass.png";
    }
}
