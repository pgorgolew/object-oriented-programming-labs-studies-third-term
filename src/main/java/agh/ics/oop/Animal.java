package agh.ics.oop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class Animal extends AbstractWorldMapElement{
    private MapDirection orient = MapDirection.NORTH;
    private final IWorldMap map;
    private final List<IPositionChangeObserver> observersList= new ArrayList<>();

    // used to test Animal behavior
    public Animal() {
        this.map = new RectangularMap(5, 5);
        this.position = new Vector2d(2, 2);
        this.addObserver((IPositionChangeObserver) this.map);
    }

    public Animal(IWorldMap map, Vector2d initialPosition){
        this.map = map;
        this.position = initialPosition;
        this.addObserver((IPositionChangeObserver) this.map);
    }

    public Vector2d getPosition() {
        return position;
    }

    public MapDirection getOrient() {
        return orient;
    }

    public void setOrient(MapDirection orient) {
        this.orient = orient;
    }

    public void setPosition(Vector2d position) {
        this.positionChanged(Vector2d.getCopy(this.position), position);
        this.position = position;
    }

    @Override
    public String toString(){
        return switch (this.orient){
            case NORTH -> "^";
            case SOUTH -> "v";
            case EAST -> ">";
            case WEST -> "<";
        };
    }

    public void move(MoveDirection direction){
        if (direction == MoveDirection.FORWARD && !map.canMoveTo(this.position.add(orient.toUnitVector()))) return;
        if (direction == MoveDirection.BACKWARD && !map.canMoveTo(this.position.subtract(orient.toUnitVector()))) return;

        switch (direction){
            case LEFT -> setOrient(this.orient.previous());
            case RIGHT -> setOrient(this.orient.next());
            case FORWARD -> setPosition(this.position.add(orient.toUnitVector()));
            case BACKWARD -> setPosition(this.position.subtract(orient.toUnitVector()));
        }
    }

    // move with list of MoveDirection values to testing
    public void move(List<MoveDirection> directions){
        for (MoveDirection direction: directions) this.move(direction);
    }

    void addObserver(IPositionChangeObserver observer){
        this.observersList.add(observer);
    }

    void removeObserver(IPositionChangeObserver observer){
        this.observersList.remove(observer);
    }

    void positionChanged(Vector2d oldPosition, Vector2d newPosition){
        for (IPositionChangeObserver observer : this.observersList){
            observer.positionChanged(oldPosition, newPosition);
        }
    }

    @Override
    public String getPathToImage() {
        if (this.toString().equals("^")) {
            return "src/main/resources/up.png";
        } else if (this.toString().equals("v")) {
            return "src/main/resources/down.png";
        } else if (this.toString().equals("<")) {
            return "src/main/resources/left.png";
        } else {
            return "src/main/resources/right.png";
        }
    }
}
