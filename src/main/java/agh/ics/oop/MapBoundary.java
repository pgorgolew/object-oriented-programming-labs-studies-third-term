package agh.ics.oop;

import java.util.SortedSet;
import java.util.TreeSet;

public class MapBoundary implements IPositionChangeObserver {
    public final SortedSet<Vector2d> mapElementsX = new TreeSet<>((o1, o2) -> {
        if (o1.equals(o2)) return 0;
        if (o1.x > o2.x) return 1;
        else if (o1.x < o2.x) return -1;
        else {
            if (o1.y > o2.y) return 1;
            return -1;
        }
    });
    private final SortedSet<Vector2d> mapElementsY = new TreeSet<>((o1, o2) -> {
        if (o1.equals(o2)) return 0;
        if (o1.y > o2.y) return 1;
        else if (o1.y < o2.y) return -1;
        else {
            if (o1.x > o2.x) return 1;
            return -1;
        }
    });
    private final AbstractWorldMap map;

    public MapBoundary(AbstractWorldMap map) {
        this.map = map;
    }

    public Vector2d getLowerLeft(){
        if (this.mapElementsX.size() == 0) return new Vector2d(0,0);

        Vector2d fromX = this.mapElementsX.first();
        Vector2d fromY = this.mapElementsY.first();
        return fromX.lowerLeft(fromY);
    }

    public Vector2d getUpperRight(){
        if (this.mapElementsX.size() == 0) return new Vector2d(0,0);

        Vector2d fromX = this.mapElementsX.last();
        Vector2d fromY = this.mapElementsY.last();
        return fromX.upperRight(fromY);
    }

    public void addElement(Vector2d position) {
        this.mapElementsX.add(position);
        this.mapElementsY.add(position);
    }

    public void removeElement(Vector2d position) {
        this.mapElementsX.remove(position);
        this.mapElementsY.remove(position);
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition){
        System.out.println(oldPosition);
        this.removeElement(oldPosition);

        this.addElement(newPosition);
    }
}
