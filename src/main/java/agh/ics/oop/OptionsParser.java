package agh.ics.oop;

import java.util.ArrayList;
import java.util.List;

public class OptionsParser {
    public static List<MoveDirection> parse(String[] commandLineArgs){
        ArrayList<MoveDirection> directionsArray = new ArrayList<>();
        for(String move : commandLineArgs) {
            switch (move) {
                case "forward", "f" -> directionsArray.add(MoveDirection.FORWARD);
                case "backward", "b" -> directionsArray.add(MoveDirection.BACKWARD);
                case "right", "r" -> directionsArray.add(MoveDirection.RIGHT);
                case "left", "l" -> directionsArray.add(MoveDirection.LEFT);
                default -> throw new IllegalArgumentException(move + " is not legal move specification");
            }
        }
        return directionsArray;
    }
}
