package agh.ics.oop;
import java.lang.Math;
import java.util.*;

public class GrassField extends AbstractWorldMap implements IWorldMap{
    private final int maxXorY;
    private final MapBoundary mapBoundary;

    public GrassField(int numberOfGrass){
        this.mapBoundary = new MapBoundary(this);
        this.mapVisualizer = new MapVisualizer(this);
        this.maxXorY = (int) Math.sqrt(numberOfGrass*10);
        placeGrasses(numberOfGrass);
    }

    private void placeGrasses(int numberOfGrass) {
        Random rand = new Random();
        while (numberOfGrass > 0) {
            int x = rand.nextInt(maxXorY + 1);
            int y = rand.nextInt(maxXorY + 1);
            Vector2d grassPosition = new Vector2d(x, y);

            if (!(isOccupied(grassPosition))) {
                Grass currGrass =  new Grass(grassPosition);
                this.allElements.put(grassPosition, currGrass);
                this.mapBoundary.addElement(currGrass.getPosition());
                numberOfGrass--;
            }
        }
    }

    public List<Grass> getAllGrasses() {
        List<Object> allGrassesObjects = super.getObjects(Grass.class);
        List<Grass> allGrasses = new ArrayList<>();
        for (Object grassObject : allGrassesObjects){
            allGrasses.add((Grass) grassObject);
        }

        return allGrasses;
    }

    @Override
    public boolean canMoveTo(Vector2d position){
        boolean ifCan = super.canMoveTo(position);
        if (!ifCan) return false;

        AbstractWorldMapElement objectAtPosition = (AbstractWorldMapElement) objectAt(position);
        if (objectAtPosition instanceof Grass) {
            this.placeGrasses(1);
            this.allElements.remove(objectAtPosition.getPosition());
            this.mapBoundary.removeElement(objectAtPosition.getPosition());
        }
        return true;
    }

    @Override
    public boolean place(Animal animal){
        AbstractWorldMapElement objectAtPosition = (AbstractWorldMapElement) objectAt(animal.getPosition());
        if (objectAtPosition instanceof Grass) {
            this.placeGrasses(1);
            this.mapBoundary.removeElement(objectAtPosition.getPosition());
            this.allElements.remove(objectAtPosition.getPosition());
        }

        super.place(animal);
        this.mapBoundary.addElement(animal.getPosition());
        return true;
    }

    public void updateCorners(){
        System.out.println(this.mapBoundary.mapElementsX);
        this.leftDown = this.mapBoundary.getLowerLeft();
        this.rightUpper = this.mapBoundary.getUpperRight();
    }

    @Override
    public String toString() {
        updateCorners();
        return super.toString();
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition) {
        AbstractWorldMapElement object = this.allElements.get(oldPosition);
        this.allElements.put(newPosition, object);
        this.mapBoundary.positionChanged(oldPosition, newPosition);
        this.allElements.remove(oldPosition);
    }
}
