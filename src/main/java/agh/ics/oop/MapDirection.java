package agh.ics.oop;

public enum MapDirection {
    NORTH,
    SOUTH,
    WEST,
    EAST;

    @Override
    public String toString(){
        switch (this) {
            case NORTH -> {
                return "Północ";
            }
            case SOUTH -> {
                return "Południe";
            }
            case WEST -> {
                return "Zachód";
            }
            case EAST -> {
                return "Wschód";
            }
        }
        return ""; //Default
    }

    public MapDirection next(){
        return switch(this) {
            case NORTH -> EAST;
            case SOUTH -> WEST;
            case WEST -> NORTH;
            case EAST -> SOUTH;
        };
    }

    public MapDirection previous(){
        return switch(this) {
            case NORTH -> WEST;
            case SOUTH -> EAST;
            case WEST -> SOUTH;
            case EAST -> NORTH;
        };
    }

    public Vector2d toUnitVector(){
        int x = 0;
        int y = 0;

        switch (this){
            case NORTH -> y = 1;
            case SOUTH -> y = -1;
            case WEST -> x = -1;
            case EAST -> x = 1;
        }
        return new Vector2d(x, y);
    }
}
