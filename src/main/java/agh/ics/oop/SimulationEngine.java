package agh.ics.oop;

import agh.ics.oop.gui.App;

import java.util.ArrayList;
import java.util.List;

public class SimulationEngine implements Runnable, IEngine {
    private List<MoveDirection> directions = new ArrayList<>();
    private final AbstractWorldMap map;
    private final List<Animal> allAnimals;
    private final App observer;
    private final int moveDelay = 2000;

    public List<Animal> getAllAnimals() {
        return this.allAnimals;
    }

    public IWorldMap getMap() {
        return this.map;
    }

    public SimulationEngine(List<MoveDirection> directions, AbstractWorldMap map, List<Vector2d> initialPositions,
                            App observer){
        this.directions = directions;
        this.map = map;
        this.allAnimals = new ArrayList<>();
        this.observer = observer;

        for (Vector2d initPosition: initialPositions){
            Animal currAnimal = new Animal(this.map, initPosition);
            this.allAnimals.add(currAnimal);
            this.map.place(currAnimal);
        }
    }

    public SimulationEngine(AbstractWorldMap map, List<Vector2d> initialPositions,
                            App observer) {
        this.map = map;
        this.allAnimals = new ArrayList<>();
        this.observer = observer;

        for (Vector2d initPosition : initialPositions) {
            Animal currAnimal = new Animal(this.map, initPosition);
            this.allAnimals.add(currAnimal);
            this.map.place(currAnimal);
        }
    }

    @Override
    public void run() {
        int numberOfAnimals = this.allAnimals.size();
        System.out.println(this.map);
        for (int i = 0; i < this.directions.size(); i++){
            Vector2d currPosition = this.allAnimals.get(i % numberOfAnimals).getPosition();
            Animal currAnimal = (Animal) this.map.objectAt(currPosition);
            MoveDirection currDirection = this.directions.get(i);
            currAnimal.move(currDirection);
            System.out.println(this.map);
            if (this.observer != null) this.updateMap();
        }
    }

    public void updateMap(){
        this.observer.show();

        try {
            Thread.sleep(this.moveDelay);
        } catch (InterruptedException e) {
            System.out.println("The simulation has stopped");
        }
    }

    public void setMoveDirectionList(List<MoveDirection> MoveDirectionsList){
        this.directions = MoveDirectionsList;
    }
}

