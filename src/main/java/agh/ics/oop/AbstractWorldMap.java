package agh.ics.oop;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractWorldMap implements IWorldMap,IPositionChangeObserver{
    protected final Map<Vector2d, AbstractWorldMapElement> allElements = new LinkedHashMap<>();

    public Vector2d getRightUpper() {
        return rightUpper;
    }

    public Vector2d getLeftDown() {
        return leftDown;
    }

    protected Vector2d rightUpper = new Vector2d(0,0);
    protected Vector2d leftDown = new Vector2d(0,0);
    protected MapVisualizer mapVisualizer;

    protected List<Object> getObjects(Class objClass){
        List<Object> allObjects = new ArrayList<>();
        for (AbstractWorldMapElement mapElement: this.allElements.values()){
            if (mapElement.getClass().equals(objClass)) allObjects.add(mapElement);
        }
        return allObjects;
    }

    @Override
    public boolean place(Animal animal) {
        if (!this.canMoveTo(animal.getPosition()))
            throw new IllegalArgumentException(animal.getPosition() + " is occupied by another animal");

        this.allElements.put(animal.getPosition(), animal);
        return true;
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        return !(objectAt(position) instanceof Animal);
    }

    @Override
    public boolean isOccupied(Vector2d position) {
        return objectAt(position) != null;
    }

    @Override
    public Object objectAt(Vector2d position) {
        return this.allElements.get(position);
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition){
        AbstractWorldMapElement object = this.allElements.get(oldPosition);
        this.allElements.remove(oldPosition);
        this.allElements.put(newPosition, object);
    }

    @Override
    public String toString() {
        return this.mapVisualizer.draw(this.leftDown, this.rightUpper);
    }
}
