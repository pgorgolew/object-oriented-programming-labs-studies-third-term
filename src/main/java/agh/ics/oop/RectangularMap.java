package agh.ics.oop;

public class RectangularMap extends AbstractWorldMap implements IWorldMap {
    private final int width;
    private final int height;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public RectangularMap(int width, int height){
        this.width = width;
        this.height = height;
        this.rightUpper =  new Vector2d(this.width-1,this.height-1);
        this.mapVisualizer = new MapVisualizer(this);
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        if (!this.rightUpper.follows(position)) return false;
        if (!this.leftDown.precedes(position)) return false;
        return super.canMoveTo(position);
    }
}
