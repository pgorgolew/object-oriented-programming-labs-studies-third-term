package agh.ics.oop.gui;

import agh.ics.oop.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App extends Application{
    GridPane grid = new GridPane();
    SimulationEngine engine;
    GrassField map;

    public void init() {
        this.map = new GrassField(10);
        List<Vector2d> positions = new ArrayList<>(Arrays.asList(new Vector2d(0, 0), new Vector2d(4, 2)));
        this.engine = new SimulationEngine(this.map, positions, this);
    }

    public void start(Stage primaryStage) {
        TextField animalMoves = new TextField();
        Button startButton = new Button("Start simulation");
        HBox userInterface = new HBox(animalMoves, startButton);

        showMap();
        VBox vBox = new VBox(this.grid, userInterface);
        vBox.setAlignment(Pos.CENTER);
        Scene scene = new Scene(vBox ,600, 600);
        primaryStage.setScene(scene);
        primaryStage.show();

        startButton.setOnAction(click -> {
            String[] args = animalMoves.getText().split(" ");
            List<MoveDirection> moveDirectionList = OptionsParser.parse(args);
            this.engine.setMoveDirectionList(moveDirectionList);
            Thread engineTread = new Thread(this.engine);
            engineTread.start();
        });
    }

    public void showMap() {
        this.map.updateCorners();
        this.grid.setGridLinesVisible(true);
        Vector2d lowerDown = this.map.getLeftDown();
        Vector2d rightUpper = this.map.getRightUpper();
        int height = rightUpper.getY() - lowerDown.getY() + 1;
        int width = rightUpper.getX() - lowerDown.getX() + 1;

        for (int y = 0; y <= height; y++) {
            for (int x = 0; x <= width; x++) {

                VBox box = getVBox(lowerDown, rightUpper, x, y);
                box.setAlignment(Pos.CENTER);
                this.grid.add(box, x, y, 1, 1);
                GridPane.setHalignment(box, HPos.CENTER);
            }
        }
        for (int y = 0; y <= height; y++) {
            this.grid.getRowConstraints().add(new RowConstraints(40));
        }
        for (int x = 0; x <= width; x++) {
            this.grid.getColumnConstraints().add(new ColumnConstraints(40));
        }
    }

    public VBox getVBox(Vector2d lowerDown, Vector2d rightUpper, int x, int y) {
        VBox resultVBox;
        int currX = lowerDown.getX() + x - 1;
        int currY = rightUpper.getY() - y + 1;
        if (x == 0 && y == 0) {
            resultVBox = new VBox(new Label("y/x"));
        } else if (x == 0) {
            resultVBox = new VBox(new Label(String.valueOf(currY)));
        } else if (y == 0) {
            resultVBox = new VBox(new Label(String.valueOf(currX)));
        } else if (this.map.isOccupied(new Vector2d(currX, currY))) {
            AbstractWorldMapElement mapElement = (AbstractWorldMapElement) this.map.objectAt(new Vector2d(currX, currY));
            String description = this.getDescription(mapElement);
            GuiElementBox guiBox = new GuiElementBox(mapElement);
            resultVBox = new VBox(guiBox.GetElementImageView(), new Label(description));
        } else {
            resultVBox = new VBox();
        }
        return resultVBox;
    }

    public String getDescription(AbstractWorldMapElement mapElement){
        if (mapElement instanceof Grass) return "Trawa";

        return "Z " + mapElement.getPosition().toString();
    }

    public void show() {
        Platform.runLater(() -> {
            this.grid.getChildren().clear();
            this.grid.getRowConstraints().clear();
            this.grid.getColumnConstraints().clear();
            this.grid.setGridLinesVisible(false);
            showMap();
        });
    }
}