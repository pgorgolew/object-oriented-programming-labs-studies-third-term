package agh.ics.oop.gui;

import agh.ics.oop.IMapElement;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class GuiElementBox {
    private final IMapElement mapElement;

    public GuiElementBox(IMapElement givenElement){
        this.mapElement = givenElement;
    }

    public ImageView GetElementImageView() {
        Image image = null;
        try {
            image = new Image(new FileInputStream(this.mapElement.getPathToImage()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);

        return imageView;
    }


}
